#!/usr/bin/env python3

import sys
from ase import io
from ase.cell import Cell

from amlt import safe_kgrid_from_cell_volume
import numpy as np



kpds = [500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000 ]
divider = ' -- ' 

# this makes printing a little nicer
kpd_col_width = len(str(max(kpds)))





######### this function was originally part of the AMLT-Auto trainer package 

def safe_kgrid_from_cell_volume(atoms, kpoint_density):
    # tries to keep equi-planar spacing in k-space to match a KPD
    import numpy as np
    kpd = kpoint_density
    lengths_angles = atoms.cell.cellpar()
    vol = atoms.get_volume()
    lengths = lengths_angles[0:3]
    ngrid = kpd/vol # BZ volume = 1/cell volume (without 2pi factors)
    plane_density_mean = (ngrid * lengths[0] * lengths[1] * lengths[2]) ** (1 / 3)

    nkpt_frac  = np.zeros(3)
    for i, l in enumerate(lengths):
        nkpt_frac[i] = max(plane_density_mean / l, 1)
        
    nkpt       = np.floor(nkpt_frac)
    actual_kpd = vol * nkpt[0]*nkpt[1]*nkpt[2]
    
    if True:
        plane_densities = lengths*nkpt
        #print(plane_densities)
        # we want to start with the largest plane spacing, not the one closest to another integer
        check_order = np.argsort( plane_densities)
    else:
        delta_ceil = np.ceil(nkpt_frac)-nkpt_frac # measure of which axes are closer to a whole number
        check_order = np.argsort(delta_ceil) # we do this so we keep the grid as even as possible only rounding up when they are close


    i = 0
    if actual_kpd < kpd:
        if np.isclose(nkpt_frac[check_order[0]], nkpt_frac[check_order[1]]) and np.isclose(nkpt_frac[check_order[1]], nkpt_frac[check_order[2]]):
            nkpt[check_order[0]] = nkpt[check_order[0]] +1
            nkpt[check_order[1]] = nkpt[check_order[1]] +1
            nkpt[check_order[2]] = nkpt[check_order[2]] +1
            actual_kpd = vol * nkpt[0]*nkpt[1]*nkpt[2]
            i = 3

        elif np.isclose(nkpt_frac[check_order[0]], nkpt_frac[check_order[1]]):
            nkpt[check_order[0]] = nkpt[check_order[0]] +1
            nkpt[check_order[1]] = nkpt[check_order[1]] +1
            actual_kpd = vol * nkpt[0]*nkpt[1]*nkpt[2]
            i = 2

        elif np.isclose(nkpt_frac[check_order[1]], nkpt_frac[check_order[2]]):
            nkpt[check_order[0]] = nkpt[check_order[0]] +1
            actual_kpd = vol * nkpt[0]*nkpt[1]*nkpt[2]
            if actual_kpd < kpd:
                nkpt[check_order[1]] = nkpt[check_order[1]] +1
                nkpt[check_order[2]] = nkpt[check_order[2]] +1
                actual_kpd = vol * nkpt[0]*nkpt[1]*nkpt[2]
            i = 3

    while actual_kpd < kpd and i<=2:
        nkpt[check_order[i]] = nkpt[check_order[i]] +1
        actual_kpd = vol * nkpt[0]*nkpt[1]*nkpt[2]
        i+=1

    kp_as_ints = [int(nkpt[i]) for i in range(3)]
    return kp_as_ints


######################### now we just lopp over goals 


def kpd_print(atoms,kpd):
    kpts = safe_kgrid_from_cell_volume(atoms, kpd)
    final_kpd = volume*np.prod(kpts)
    kpd_gridplane_density = np.asarray(kpts)*cell.lengths()
    ngrid_target = kpd/volume
    kpd_gridplane_density_target = np.cbrt( ngrid_target  * np.prod(cell.lengths()) )

    final_kpd_str = ('%.1f'%final_kpd).rjust(kpd_col_width+2)
    print('kpd', str(kpd).rjust(kpd_col_width),
        'kpts', kpts,
        '[final kpd %s]'%final_kpd_str, 
        'gridplane kpd (target from kpd) [ %.2f, %.2f, %.2f]'%tuple( kpd_gridplane_density ),
        '(%.2f)'%kpd_gridplane_density_target )

fnames = sys.argv[1:]
if len(sys.argv) >= 2:
    
    for fi, fname in enumerate(fnames):
        try:
            atoms = io.read(fname)
            file_read_ok = True
        except:
            file_read_ok = False
        
        if file_read_ok:
            print('k-points for "%s":'%fname)
            
            volume = atoms.get_volume()
            cell = atoms.cell.complete()
            rcell = Cell(atoms.cell.reciprocal())
            
            print('Cell Parameters and Vectors:')
            print(cell.lengths(), cell.angles())
            print(np.asarray(atoms.cell))
            print('\nR-Cell Parameters and Vectors:')
            print(rcell.lengths(), rcell.angles())
            print(np.asarray(rcell))
            
            print('\nk-point Suggestions:')
            for kpd in kpds:

                 kpd_print(atoms,kpd)

#                kpts = safe_kgrid_from_cell_volume(atoms, kpd)
#                final_kpd = volume*np.prod(kpts)
#                kpd_gridplane_density = np.asarray(kpts)*cell.lengths()
#                ngrid_target = kpd/volume
#                kpd_gridplane_density_target = np.cbrt( ngrid_target  * np.prod(cell.lengths()) )

#                final_kpd_str = ('%.1f'%final_kpd).rjust(kpd_col_width+2)
#                print('kpd', str(kpd).rjust(kpd_col_width),
#                    'kpts', kpts,
#                    '[final kpd %s]'%final_kpd_str, 
#                    'gridplane kpd (target from kpd) [ %.2f, %.2f, %.2f]'%tuple( kpd_gridplane_density ),
#                    '(%.2f)'%kpd_gridplane_density_target )
            
        elif fname.isnumeric():
            kpd = float(fname)
            kpd_print(atoms, kpd)
        else:
            print('"%s" failed to read!'%fname)


        if fi < (len(fnames)-1) and len(divider)>0:  print(divider)
else:
    print('No atomic structure file(s) given!')




    
