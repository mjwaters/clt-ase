#!/usr/bin/env python3

# for testing with tolerances larger than 0.1, a customizable list of non-powers of 10 is defined here.
large_symprecs = [2.0,1.0,0.5]
divider = ' -- '
log10_symprecs = [ 10.0**power  for power in range(-1,-9, -1)]

# ----------- some tidy functions
import sys
from ase import io
from ase.spacegroup import get_spacegroup
import os

def print_lattice_header(atoms):
    print(atoms.cell.get_bravais_lattice())
    print('  |a|,  |b|,   |c| : [{: 16.10f}  {: 16.10f}  {: 16.10f}]'.format(*atoms.cell.lengths()))
    print('alpha, beta, gamma : [{: 16.10f}  {: 16.10f}  {: 16.10f}]'.format(*atoms.cell.angles()))
    print('a : [{: 16.10f}  {: 16.10f}  {: 16.10f}]'.format(atoms.cell[0,0], atoms.cell[0,1], atoms.cell[0,2]))
    print('b : [{: 16.10f}  {: 16.10f}  {: 16.10f}]'.format(atoms.cell[1,0], atoms.cell[1,1], atoms.cell[1,2]))
    print('c : [{: 16.10f}  {: 16.10f}  {: 16.10f}]'.format(atoms.cell[2,0], atoms.cell[2,1], atoms.cell[2,2]))
    #print('a : [{: 4.10f}  {: 4.10f}  {: 4.10f}]'.format(tuple(atoms.cell[0])))
    #print('b : '%tuple(atoms.cell[1]))
    #print('c : '%tuple(atoms.cell[2]))

def print_spg_line(symprec, spg):
    '''printing function for each symprec level test.'''
    spg_num_str = ('#'+str(spg.no)).rjust(4)
    dict_yn_bool = {True: 'yes', False: 'no'}
    centro_string = dict_yn_bool[spg.centrosymmetric].ljust(3)
    print("[symprec = %.1e] -> {centsym: %s} (%s) %s "%(symprec, centro_string, spg_num_str,  spg.symbol ))

def old_print_spg_line(symprec, spg):
    '''printing function for each symprec level test.'''
    spg_num_str = str(spg.no).rjust(3)
    print("[symprec = %.1e] -> (%s) %s "%(symprec, spg_num_str,  spg.symbol ))

def read_safe(fname):
    if not os.path.isfile(fname):
        readable=False
        nofilewarning='File not found: "%s"'%fname
        print(nofilewarning)
    else:
        try:
            atoms = io.read(fname)
            readable=True
        except:
            readable=False
            not_readable_warning='"%s" is not readable by ASE.'%fname,
            print(not_readable_warning)

    if not readable:
        atoms=None
    return  readable, atoms

#------------- excution starts here
if len(sys.argv) < 2:
    print('No atomic structure file(s) given!')
else :
    fnames = sys.argv[1:]
    for fi, fname in enumerate(fnames):

        readable, atoms = read_safe(fname)
        if readable:
            print('"%s" is :'%fname)
            print_lattice_header(atoms)

            for symprec in large_symprecs+log10_symprecs:
                try:
                    spg = get_spacegroup(atoms, symprec = symprec)
                    print_spg_line(symprec, spg)
                except:
                    pass

        # print a divider only when between files
        if fi < (len(fnames)-1) and len(divider)>0:  print(divider)
