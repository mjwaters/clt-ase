#!/usr/bin/env python3



from ase import Atoms
import json

def read_bandpath_from_json(fname='bandpath.json'):
    with open(fname) as f:
        datadict = json.load(f)
    special_path, special_points = datadict['special_path'], datadict['special_points']
    if 'atoms' in datadict:
        atoms = Atoms(**datadict['atoms'])
    else:
        atoms=None
    return special_path, special_points, atoms



if __name__ == "__main__":
    import sys
    from ase import io

    bandpath_filename = sys.argv[1]
    special_path, special_points, atoms = read_bandpath_from_json(bandpath_filename)

    if False: # depricated feature
        if len(sys.argv)>=3:
            atoms = io.read(sys.argv[2])
        print(atoms)
        assert atoms is not None

    if len(sys.argv)>=3: # for reading nkpts for interpolation
        nkpts = int(sys.argv[2])
    else:
        nkpts = 0
        
    ############
    from ase.dft.kpoints import BandPath

    print('special_points:')
    for key in special_points.keys():
        print(key, special_points[key])
    print('special_path:', special_path)

    bp = BandPath( cell=atoms.get_cell(),
        path=special_path, special_points=special_points )

    if nkpts>0:
        bp_interpolated = bp.interpolate(npoints=nkpts)

    #########
    from matplotlib import pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    from mpl_toolkits.mplot3d import proj3d
    fig = plt.figure(figsize=(2.5,2.5))
    ax = fig.add_subplot(projection='3d')
    if nkpts==0:
        bp.plot(ax=ax)
        fig.tight_layout(pad=0.2)
        fig.savefig( bandpath_filename+'.pdf', transparent=True, dpi=300)
    else:
        bp_interpolated.plot(ax=ax)
        fig.tight_layout(pad=0.2)
        fig.savefig( bandpath_filename+'.interpolated.pdf', transparent=True, dpi=300)
    
    plt.show()



    #########
    if False:
        fig = plt.figure(figsize=(2.5,2.5))
        ax = fig.add_subplot(projection='3d')
        bp_interpolated.plot(ax=ax)
        fig.tight_layout(pad=0.2)
        fig.savefig( bandpath_filename+'.interpolated.pdf', transparent=True, dpi=300)
        plt.show()
