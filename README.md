# CLT-ASE

Command Line Tools built on ASE.

## Installation

They are just executable python scripts. Put them somewhere and add that somewhere to your ```$PATH``` like this:
```bash
export PATH=$PATH:$HOME/software/clt-ase
```

## asepath.py

Analyzes a sequences of structures and gives info about the path. 

`E` is energy. If energy/force infomation is not available, zeros will be given.

`dE` is the change in energy between structures.

`rmsF` is root-mean-square-force on the atoms. The largest atomic force and the offending atom index are shown in parentheses. 

`dX` is the L2 distance between the structures. 

`|X|` is the sum of `dX` or the net path length that might be seen in NEB. 

`fp` is the scalar projection of current step on the previous step normalized to the previous step size which lets you quickly see if the steps are moving forward monotonically and their relative sizes. 

`∠`, the last item, is the angle between the current and previous update vectors. Angles greater than 120° typically mean a poor structural optimization step has been performed. For NEBs, the angles will ideally be less than 60° for a smooth minimum energy pathway.

In the example, I restarted a relaxation and want to see the progression:
```
> asepath.py OUTCAR.001 OUTCAR.002 
OUTCAR.001    0 E  -19.127506130 dE   0.000000000 rmsF 4.141e-02 (5.071e-02 on    2) dX 0.000000 |X| 0.0000 fp   0.00 ∠   0.0°
OUTCAR.001    1 E  -19.127661950 dE  -0.000155820 rmsF 2.351e-02 (2.880e-02 on    2) dX 0.003877 |X| 0.0039 fp   0.00 ∠   0.0°
OUTCAR.001    2 E  -19.127685370 dE  -0.000023420 rmsF 1.761e-02 (2.157e-02 on    2) dX 0.001299 |X| 0.0052 fp   0.33 ∠   0.4°
OUTCAR.001    3 E  -19.127700780 dE  -0.000015410 rmsF 5.868e-03 (7.186e-03 on    2) dX 0.002597 |X| 0.0078 fp   2.00 ∠   0.5°
OUTCAR.001    4 E  -19.127701130 dE  -0.000000350 rmsF 7.198e-03 (8.816e-03 on    2) dX 0.000302 |X| 0.0081 fp  -0.12 ∠ 179.5°
OUTCAR.001    5 E  -19.127714720 dE  -0.000013590 rmsF 1.140e-03 (1.396e-03 on    2) dX 0.001823 |X| 0.0099 fp   3.30 ∠  56.9°
 -- 
OUTCAR.002    0 E  -19.113555610 dE   0.014159110 rmsF 2.362e-01 (2.685e-01 on    2) dX 0.092763 |X| 0.1027 fp  22.71 ∠  63.5°
OUTCAR.002    1 E  -19.118753080 dE  -0.005197470 rmsF 1.802e-01 (1.988e-01 on    0) dX 0.023183 |X| 0.1258 fp  -0.18 ∠ 137.2°
OUTCAR.002    2 E  -19.121659960 dE  -0.002906880 rmsF 1.113e-01 (1.564e-01 on    0) dX 0.040723 |X| 0.1666 fp   1.76 ∠   0.1°
OUTCAR.002    3 E  -19.121723260 dE  -0.000063300 rmsF 1.160e-01 (1.607e-01 on    0) dX 0.005051 |X| 0.1716 fp  -0.12 ∠ 179.8°
OUTCAR.002    4 E  -19.124042890 dE  -0.002319630 rmsF 9.825e-02 (1.384e-01 on    0) dX 0.012701 |X| 0.1843 fp   0.06 ∠  88.7°
OUTCAR.002    5 E  -19.125051150 dE  -0.001008260 rmsF 8.612e-02 (1.130e-01 on    0) dX 0.015722 |X| 0.2000 fp   1.24 ∠   0.1°
OUTCAR.002    6 E  -19.126220080 dE  -0.001168930 rmsF 6.674e-02 (8.226e-02 on    0) dX 0.009903 |X| 0.2099 fp   0.14 ∠  77.2°
OUTCAR.002    7 E  -19.127466730 dE  -0.001246650 rmsF 3.464e-02 (4.342e-02 on    1) dX 0.024434 |X| 0.2344 fp   2.47 ∠   0.1°
```

If you are working with NEBs, you might be interested in seeing the distance between structures after removing the translation between them. The `-rt` flag will do this. Note the `dX` in this example:

```
> asepath.py ref.POSCAR CONTCAR
ref.POSCAR    0 E    0.000000000 dE   0.000000000 rmsF 0.000e+00 (0.000e+00 on    0) dX 0.000000 |X| 0.0000 fp   0.00 ∠   0.0°
 -- 
CONTCAR    0 E    0.000000000 dE   0.000000000 rmsF 0.000e+00 (0.000e+00 on    0) dX 0.713708 |X| 0.7137 fp   0.00 ∠   0.0°
> asepath.py -rt ref.POSCAR CONTCAR
ref.POSCAR    0 E    0.000000000 dE   0.000000000 rmsF 0.000e+00 (0.000e+00 on    0) dX 0.000000 |X| 0.0000 fp   0.00 ∠   0.0°
 -- 
CONTCAR    0 E    0.000000000 dE   0.000000000 rmsF 0.000e+00 (0.000e+00 on    0) dX 0.516530 |X| 0.5165 fp   0.00 ∠   0.0°
```


## asesym.py

Gives symmetry info at different tolerance levels. ASE will need SPGlib for this. Ex:

```
> asesym.py CONTCAR 
"CONTCAR" is :
MCL(a=3.21592, b=5.31034, c=6.09771, alpha=88.9783)
a : [   -3.2159212845      0.0034901910     -0.0002611591]
b : [    0.0042659587      4.3720891597      4.2505158758]
c : [    0.0047272913      3.7689722585     -3.7409339584]
[symprec = 2.0e+00] -> ( 38) A m m 2
[symprec = 1.0e+00] -> (  6) P m
[symprec = 5.0e-01] -> (  6) P m
[symprec = 1.0e-01] -> (  6) P m
[symprec = 1.0e-02] -> (  6) P m
[symprec = 1.0e-03] -> (  6) P m
[symprec = 1.0e-04] -> (  1) P 1
[symprec = 1.0e-05] -> (  1) P 1
[symprec = 1.0e-06] -> (  1) P 1
[symprec = 1.0e-07] -> (  1) P 1
[symprec = 1.0e-08] -> (  1) P 1
```

## aseresym.py

Starting by looking at the symmetry of a structure:

```
> asesym.py CONTCAR.min 
"CONTCAR.min" is :
MCLC(a=2.67735, b=4.38585, c=8.99082, alpha=77.8549)
  |a|,  |b|,   |c| : [    2.5691461329      2.5693251141      8.9908236957]
alpha, beta, gamma : [   79.6554491927     79.6595394920     62.7995053319]
a : [   -1.8726617014      0.0202620846     -1.7587607718]
b : [    0.0202984124     -1.8728736953     -1.7587960763]
c : [   -5.7831205398     -5.7836630679      3.7335866224]
[symprec = 1.0e+00] -> (160) R 3 m
[symprec = 5.0e-01] -> (160) R 3 m
[symprec = 1.0e-01] -> (  8) C m
[symprec = 1.0e-02] -> (  8) C m
[symprec = 1.0e-03] -> (  8) C m
[symprec = 1.0e-04] -> (  1) P 1
[symprec = 1.0e-05] -> (  1) P 1
[symprec = 1.0e-06] -> (  1) P 1
[symprec = 1.0e-07] -> (  1) P 1
[symprec = 1.0e-08] -> (  1) P 1
```

We can pick a symprec of 1e-3 (default of 1e-2) to resymmetrize the structure:

```
> aseresym.py CONTCAR.min CONTCAR.resym 1e-3
"CONTCAR.min" has been refinded in "CONTCAR.resym" as :
MCLC(a=2.67717, b=4.38596, c=8.99082, alpha=77.858)
a : [   -1.8727059844      0.0202178015     -1.7588448577]
b : [    0.0203426954     -1.8728294121     -1.7587119903]
c : [   -5.7835205993     -5.7832630293      3.7335866052]
[symprec = 1.0e-03] -> (  8) C m
```

and we can check the final structure's symmetry:

```
> asesym.py CONTCAR.resym 
"CONTCAR.resym" is :
MCLC(a=2.67717, b=4.38596, c=8.99082, alpha=77.858)
  |a|,  |b|,   |c| : [    2.5692356251      2.5692356251      8.9908236957]
alpha, beta, gamma : [   79.6574942841     79.6574942841     62.7995054033]
a : [   -1.8727059844      0.0202178015     -1.7588448577]
b : [    0.0203426954     -1.8728294121     -1.7587119903]
c : [   -5.7835205993     -5.7832630293      3.7335866052]
[symprec = 1.0e+00] -> (160) R 3 m
[symprec = 5.0e-01] -> (160) R 3 m
[symprec = 1.0e-01] -> (  8) C m
[symprec = 1.0e-02] -> (  8) C m
[symprec = 1.0e-03] -> (  8) C m
[symprec = 1.0e-04] -> (  8) C m
[symprec = 1.0e-05] -> (  8) C m
[symprec = 1.0e-06] -> (  8) C m
[symprec = 1.0e-07] -> (  8) C m
[symprec = 1.0e-08] -> (  8) C m
```

## asekpd.py

A tool for suggesting k-grids, currently requires AMLT for the k-grids (https://github.com/MTD-group/amlt/blob/master/kgrid.py#L4) which will not be required in the future:

```
> asekpd.py CONTCAR
k-points for "CONTCAR":
Cell Parameters and Vectors:
[2.53236518 2.53228849 8.63605187] [71.62366428 71.62875356 85.62870252]
[[-2.14165409  0.2917771  -1.31949129]
 [ 0.29218798 -2.1421964  -1.31837239]
 [-5.76337252 -5.76241983  2.85647454]]

R-Cell Parameters and Vectors:
[0.41623171 0.41625659 0.1282366 ] [107.92490104 107.91967444  88.52768695]
[[-0.27508677  0.13564955 -0.28138107]
 [ 0.13577721 -0.27521022 -0.28123555]
 [-0.06440445 -0.06435946  0.09030255]]

k-point Suggestions:
kpd    500 kpts [3, 4, 1] [final kpd    598.3] gridplane kpd (target from kpd) [ 7.60, 10.13, 8.64] (8.22)
kpd   1000 kpts [4, 4, 2] [final kpd   1595.6] gridplane kpd (target from kpd) [ 10.13, 10.13, 17.27] (10.36)
kpd   2000 kpts [5, 5, 2] [final kpd   2493.1] gridplane kpd (target from kpd) [ 12.66, 12.66, 17.27] (13.05)
kpd   5000 kpts [7, 7, 3] [final kpd   7329.6] gridplane kpd (target from kpd) [ 17.73, 17.73, 25.91] (17.71)
kpd  10000 kpts [8, 9, 3] [final kpd  10770.0] gridplane kpd (target from kpd) [ 20.26, 22.79, 25.91] (22.31)
kpd  20000 kpts [11, 11, 4] [final kpd  24132.8] gridplane kpd (target from kpd) [ 27.86, 27.86, 34.54] (28.11)
kpd  50000 kpts [15, 15, 5] [final kpd  56093.8] gridplane kpd (target from kpd) [ 37.99, 37.98, 43.18] (38.15)
kpd 100000 kpts [18, 19, 6] [final kpd 102315.1] gridplane kpd (target from kpd) [ 45.58, 48.11, 51.82] (48.07)
kpd 200000 kpts [24, 24, 7] [final kpd 201040.2] gridplane kpd (target from kpd) [ 60.78, 60.77, 60.45] (60.56)
kpd 500000 kpts [32, 32, 10] [final kpd 510578.3] gridplane kpd (target from kpd) [ 81.04, 81.03, 86.36] (82.20)
```

## aseirkgrid.py




