#!/usr/bin/env python3

rotate_to_standard_orientation = False

import sys
from ase import io
from ase.spacegroup import get_spacegroup
from ase.spacegroup.symmetrize import refine_symmetry


fnames = sys.argv[1:]

divider = ' -- ' 

if len(sys.argv) >= 3:
    
    filein = sys.argv[1]
    fileout = sys.argv[2]
    if len(sys.argv) >= 4:
        symprec_string = sys.argv[3] 
        symprec = float(symprec_string)
    else:
        symprec = 1e-2 # the ase default
    
    if sys.argv[-1] == '-rso' :
        rotate_to_standard_orientation= True

    ### all the magic is here
    atoms = io.read(filein)    
    refine_symmetry(atoms, symprec=symprec, verbose=False)
    if rotate_to_standard_orientation:
        newcell, romat = atoms.cell.standard_form()
        atoms.set_cell(newcell, scale_atoms=True)
    spg = get_spacegroup(atoms, symprec = symprec/2) # test with higher tolerance?
    io.write(fileout, atoms)
    ####
    
    
    print('"%s" has been refinded in "%s" as :'%(filein, fileout))
    print(atoms.cell.get_bravais_lattice())
    print('a : [{: 16.10f}  {: 16.10f}  {: 16.10f}]'.format(atoms.cell[0,0], atoms.cell[0,1], atoms.cell[0,2]))
    print('b : [{: 16.10f}  {: 16.10f}  {: 16.10f}]'.format(atoms.cell[1,0], atoms.cell[1,1], atoms.cell[1,2]))
    print('c : [{: 16.10f}  {: 16.10f}  {: 16.10f}]'.format(atoms.cell[2,0], atoms.cell[2,1], atoms.cell[2,2]))
    spg_string = str(spg.no).rjust(3)
    print("[symprec = %.1e] -> (%s)"%(symprec,spg_string),  spg.symbol)


else:
    print('Not enough atomic structure files given! 2 files are required.')




