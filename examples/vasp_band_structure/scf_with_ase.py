


primitive_file = 'Si_conventional.pmg_SC.prim_std.POSCAR'


import os
os.environ['VASP_COMMAND'] = 'mpirun -np 4 vasp_std'
os.environ['VASP_PP_PATH'] = '/home/mjwaters/vasp_pseudos'


vasp_settings = dict( 
    xc='PBE', setups='minimal' ,
    prec = 'Accurate', ediff = 1E-8, encut = 350,
    ispin = 1,  isym = 2,
    lmaxmix = 6, lasph = True, lreal = 'Auto',
    gamma = True, kpts = [4,4,4],
    ismear = 0,  sigma = 0.05,
    algo = 'Normal', amix=0.2,
    kpar = 4, ncore=1,
    )

## The VASP calculator won't even turn on +U until
## one of these atom types is found in the system
ldau_luj={  
    'Fe':{'L':2,  'U':4.0, 'J':0.0},
    'Ni':{'L':2,  'U':5.0, 'J':0.0},
            }
vasp_settings['ldau_luj'] = ldau_luj


from ase import io
from ase.calculators.vasp import Vasp

atoms=io.read(primitive_file)

atoms.calc=Vasp(directory='scf', **vasp_settings)

# runs the calculation
PE = atoms.get_potential_energy()
