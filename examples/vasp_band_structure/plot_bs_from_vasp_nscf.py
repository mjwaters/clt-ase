





bandpath_filename='bandpath.pmg_SC.prim_std.json'
plot_filename=bandpath_filename.replace('.json','.pdf')


##### plotting options
emax = 3.5
emin = -1.5

figsize=(3.2, 2.0)
legendkeywords = dict(
    ncol = 3,
    labelspacing = 0.1,
    handlelength = 1.2,
    handletextpad = 0.3,
    columnspacing = 0.5,
    borderpad    = 0.1,
    borderaxespad  = 0.3,
    fontsize = 10)

##################### fermi level options
# can be used to set the fermi level exactly to the VBM
vb_index =None #
E_ref = 0.0 # the fermi level to subtract from the energy eigen values

read_scf_for_fermi_level = True

##################### Path specification
from ase import Atoms
import json

def read_bandpath_from_json(fname='bandpath.json'):
    with open(fname) as f:
        datadict = json.load(f)
    special_path, special_points = datadict['special_path'], datadict['special_points']
    if 'atoms' in datadict:
        atoms = Atoms(**datadict['atoms'])
    else:
        atoms=None
    return special_path, special_points, atoms

special_path, special_points, atoms = read_bandpath_from_json(bandpath_filename)

################# Data ingestion
import numpy as np
from ase.calculators.vasp import Vasp

calc=Vasp(directory='nscf', restart=True)
atoms = calc.atoms

#structure_file          = 'output_scf.final.POSCAR'
#energy_eigenvalues_file = 'energies.txt'
#kpts_file               = 'k-points.txt'

#atoms = io.read( structure_file )
#kpts = np.loadtxt( kpts_file) # in the form of kpts[ Nkpts, 3] in the reduced brillouin zone
#energies = np.loadtxt( energy_eigenvalues_file) # in the form energies[Nkpts, Nbands]

#energies = 

kpts_found = calc.get_ibz_k_points()
#print(kpts_found.shape)
nkpts = kpts_found.shape[0]
energies = []
for s in range(calc.get_number_of_spins()):
    se = [calc.get_eigenvalues(kpt=k, spin=s)   for k in range(nkpts)]
    energies.append(se)
energies = np.array(energies)
#print(energies.shape)

##################### Imports and some helper functions
from ase.dft.kpoints import resolve_custom_points, find_bandpath_kinks, BandPath, parse_path_string
from ase.spectrum.band_structure import get_band_structure, BandStructure, BandStructurePlot
from ase.dft.bandgap import bandgap

def get_better_bs(calc_bs, E_ref = 0.0):
    kpts = calc_bs.get_ibz_k_points()
    energies = []
    for s in range(calc_bs.get_number_of_spins()):
        energies.append([calc_bs.get_eigenvalues(kpt=k, spin=s)
                         for k in range(len(kpts))])
    energies = np.array(energies)
    print(energies.shape)

    from ase.dft.kpoints import resolve_custom_points, find_bandpath_kinks
    from ase.spectrum.band_structure import BandStructure#, get_band_structure
    path = calc_bs.atoms.cell.bandpath(npoints=len(kpts))
    print (path)
    print (path.special_points)
    kinks = find_bandpath_kinks(calc_bs.atoms.cell, kpts, eps=1e-5)
    print(kinks)
    pathspec = resolve_custom_points(kpts[kinks], path.special_points, eps=1e-5)
    #path.kpts = kpts
    #path.path = pathspec
    bs = BandStructure(path = path, energies = energies - E_ref )
    return bs

def pretty(kpt):
    if kpt == 'G':
        kpt = r'$\Gamma$'
    elif len(kpt) == 2:
        kpt = kpt[0] + '$_' + kpt[1] + '$'
    return kpt

def fix_xticks_for_bandpath(ax, band_path, tol=1e-5):
    # produces duplicates...
    xcoords, special_xcoords, labels = band_path.get_linear_kpoint_axis()
    print(labels)
    print(special_xcoords)

    tick_xcoords = [special_xcoords[0]]
    tick_labels =  [pretty(labels[0])]

    print('~')
    index=0
    print(labels[index], special_xcoords[index])

    for index in range(1,len(labels)):
        if not labels[index-1] == labels[index]:
            print(labels[index], special_xcoords[index])
            # this is for a discontinuity
            if abs(special_xcoords[index] - special_xcoords[index-1])<tol:
                tick_labels[-1]+='|' + pretty( labels[index])
            else:
                tick_labels.append( pretty( labels[index]))
                tick_xcoords.append(special_xcoords[index])
    ax.set_xticks(     tick_xcoords)
    ax.set_xticklabels(tick_labels)
    print('~')

    ax.minorticks_on()
    ax.tick_params(axis='x', which = 'minor', bottom = False)
    ax.tick_params(axis='y', which = 'minor', left = True)


######## setting reference levels and gap stuff
# Still working on using the bangap() function without needing a calculator
if read_scf_for_fermi_level:
    print('\nSCF:')
    calc_SCF = Vasp(directory='scf', restart=True)
    ef_SCF = calc_SCF.get_fermi_level()
    gap, pv, pc = bandgap(calc_SCF)
    vb_index_scf = pv[2]
    vbm_e = calc_SCF.get_eigenvalues(spin = pv[0], kpt = pv[1])[pv[2]]
    print('Ef:', ef_SCF, 'Gap:', gap)
    E_ref = ef_SCF



if vb_index is not None:
    vbm_e = energies[:,vb_index].max()
    E_ref = vbm_e
energies = energies - E_ref



################# Make the band path from the kpts and special points
band_path = BandPath(atoms.get_cell(),
    kpts = kpts_found,
    path = special_path, ## this isn't always required, can be commented out
    special_points = special_points
    )


#### printing Info
print('band_path object:',band_path)
print('band_path object kpts:',band_path.kpts)
print('band_path.get_linear_kpoint_axis() gives:')
xcoords, special_xcoords, labels = band_path.get_linear_kpoint_axis()
print(' > xcoords:', xcoords)
print(' > special_xcoords:', special_xcoords)
print(' > labels:', labels)


#################### actual plotting section
plot_bs=True
if plot_bs:
    from matplotlib import pyplot as plt

    fig, ax = plt.subplots(figsize = figsize, dpi = 192)

    color = 'indigo'
    #print('energies', energies.shape)


    bs = BandStructure(path = band_path, energies = energies  )
    bs.plot( ax = ax , show= False, emin = emin, emax = emax, color=color)

    if False: # enable to overlay multiple band structures
        bs2 = BandStructure(path = band_path, energies = energies2  )
        bs2.plot( ax = ax , show= False, emin = emin, emax = emax, color=color)

    fix_xticks_for_bandpath(ax, band_path)
    ## auto sizes the axes inside the figure

    ax.set_ylabel('E-E$_{f}$ (eV)')
    fig.tight_layout(pad = 0.2)
    fig.savefig(plot_filename, transparent = True, dpi = 600)

    plt.show()

