

N_bandpath_kpts = 64

# stored in the bandpath.json file, so not needed
#primitive_file = 'Si_conventional.pmg_SC.prim_std.POSCAR'
bandpath_filename='bandpath.pmg_SC.prim_std.json'


import os
os.environ['VASP_COMMAND'] = 'mpirun -np 4 vasp_std'
os.environ['VASP_PP_PATH'] = '/home/mjwaters/vasp_pseudos'

## copied from SCF run
vasp_settings = dict( 
    xc='PBE', setups='minimal' ,
    prec = 'Accurate', ediff = 1E-8, encut = 350,
    ispin = 1,  isym = 2,
    lmaxmix = 6, lasph = True, lreal = 'Auto',
    gamma = True, kpts = [4,4,4],
    ismear = 0,  sigma = 0.05,
    algo = 'Normal', amix=0.2,
    kpar = 4, ncore=1,
    )

## The VASP calculator won't even turn on +U until
## one of these atom types is found in the system
ldau_luj={  
    'Fe':{'L':2,  'U':4.0, 'J':0.0},
    'Ni':{'L':2,  'U':5.0, 'J':0.0},
            }
vasp_settings['ldau_luj'] = ldau_luj


########### get the bandpath kpts

from ase import Atoms
import json

def read_bandpath_from_json(fname='bandpath.json'):
    with open(fname) as f:
        datadict = json.load(f)
    special_path, special_points = datadict['special_path'], datadict['special_points']
    if 'atoms' in datadict:
        atoms = Atoms(**datadict['atoms'])
    else:
        atoms=None
    return special_path, special_points, atoms

special_path, special_points, atoms = read_bandpath_from_json(bandpath_filename)

from ase.dft.kpoints import BandPath
bp = BandPath( cell=atoms.get_cell(),
    path=special_path, special_points=special_points )

### create interpolated bp
bp_interpolated = bp.interpolate(npoints=N_bandpath_kpts)

############ NSCF SETTINGS ###########

vasp_settings['icharg']= 11 #Run in NSCF mode
vasp_settings['lcharg']= False # don't save over the CHGCAR
vasp_settings['kpts']  = bp_interpolated.kpts
vasp_settings['reciprocal']  = True

##################################
from ase.calculators.vasp import Vasp

# read from bandpath file
#atoms=io.read(primitive_file)

atoms.calc=Vasp(directory='nscf', **vasp_settings)

# runs the calculation
PE = atoms.get_potential_energy()
