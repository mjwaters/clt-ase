#


### produces bandpath.pmg_SC.prim_std.json  and   Si_conventional.pmg_SC.prim_std.POSCAR
### this is your primitive cell and bandpath
pymatgen-bp.py Si_conventional.POSCAR 


### run the scf in the 'scf' directory
python scf_with_ase.py


### copy the CHGCAR to the new folder
mkdir -p nscf 
cp scf/CHGCAR nscf/CHGCAR


### run the nscf in the 'nscf' directory
python nscf_with_ase.py


### If you plan on running the NSCF without ASE, you can generate the KPOINTS file
### indpendently by sampling 64 points on the bandpath 
# asebandpathtovasp.py bandpath.pmg_SC.prim_std.json 64



### plotting the BS
python plot_bs_from_vasp_nscf.py 


#### plotting the band path in the BZ
aseplotbandpath.py bandpath.pmg_SC.prim_std.json

