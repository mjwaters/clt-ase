#!/usr/bin/env python3


# these should be args parsed at some point

wrap=True
with_time_reversal=True
print_full_info = False

label = 'seekpath.prim'
bandpath_filename = 'bandpath.%s.json' %label
#########
from ase import io, Atoms
import sys
from seekpath import get_path
import json
import numpy as np
import os

def make_filename(fname_in, label):
    fname = os.path.basename(fname_in) # so the json and structure go to the same place
    i = 0
    for _ in range(len(fname)):
        if fname[_]=='.':
            i=_
    outname = fname[0:i] + '.'+label+fname[i:]
    return outname


def convert_seekpath_symbol(sym):
    if sym == 'GAMMA':
        return 'G'
    elif '_' in sym:
        return sym.replace('_','')
    elif sym == 'G': # yeah there are some points G and G_2, G_4 etc, points
        #we'll catch the G and convert it to G_1 to prevent it getting converted to Gamma
        return 'G1'
    else:
        return sym

def to_ase_path_string(seekpath_path, divider = ','):

    path_list = []
    for i in range(len(seekpath_path)-1):

        seg = seekpath_path[i]
        # always add first part of segment
        path_list.append(convert_seekpath_symbol(seg[0]))
        # end of last and start of new don't match
        if seg[1] !=  seekpath_path[i+1][0]:
            path_list.append(convert_seekpath_symbol(seg[1]))
            path_list.append(divider)

    # always attach last segment since duplicate removal is handled in the
    # prior segments
    path_list.append(convert_seekpath_symbol(seekpath_path[-1][0]))
    path_list.append(convert_seekpath_symbol(seekpath_path[-1][1]))

    path_string = ''
    for point in path_list:
        path_string+=point
    return path_string

def to_ase_special_points(point_coords):
    special_points = {}
    for key in point_coords:
        special_points[convert_seekpath_symbol(key)] = point_coords[key]
    return special_points


def atoms_to_dict(atoms):
    adict =  atoms.todict()
    #'symbols'          = atoms.get_chemical_symbols()
    #'scaled_positions' = atoms.get_scaled_positions()
    #'cell'             = atoms.get_cell()
    #'pbc'              = atoms.get_pbc()

    for key in adict: # i hope this always works so I don't have to do something ugly like above
        adict[key] = adict[key].tolist()
    return adict


def write_bandpath_to_json(fname, special_path, special_points, atoms=None,
    sanitize_kpt_vectors=True):
    # ex file name ='bandpath.json'

    if sanitize_kpt_vectors:
        spout = {}
        for key in special_points:
            kpt = [float(val) for val in special_points[key] ] #force dtype of float for json
            spout[key] = kpt
    else:
        spout = special_points

    dout = {'special_path'   : special_path,
            'special_points' : spout}
    if atoms is not None:
        dout['atoms'] = atoms_to_dict(atoms)
    import json
    with open(fname, 'w') as f:
        json.dump(dout, f, indent=4,  separators=(',',':'))

# reduce duplicates
#def read_bandpath_from_json(fname='bandpath.json'):
#    with open(fname) as f:
#        d = json.load(f)
#    special_path, special_points = d['special_path'], d['special_points']
#    return special_path, special_points, atom

def print_basic_info(data):
    print('Primitive Structure Info:')
    print('volume_original_wrt_prim', ':', np.round(data['volume_original_wrt_prim'], 8) )
    print('bravais_lattice_extended', ':', data['bravais_lattice_extended'])
    print('has_inversion_symmetry', ':', data['has_inversion_symmetry'])

    print('Space Group', ':', data['spacegroup_international'],
        '(#{: 2d})'.format(data['spacegroup_number']) )

##### reading structure #######
fname=sys.argv[-1]
atoms = io.read(fname)

##### getting seekpath results ###
structure = (atoms.get_cell(), atoms.get_scaled_positions(), atoms.get_atomic_numbers())
data = get_path(structure, with_time_reversal=with_time_reversal)# symprec = 1e-4, )#angle_tolerance=0.1 )

##### converting to back to Atoms object #######
prim_cell = data['primitive_lattice']
prim_scaled_positions = data['primitive_positions']
prim_atomic_numbers   = data['primitive_types']
prim_atoms = Atoms(scaled_positions = prim_scaled_positions, numbers=prim_atomic_numbers, cell=prim_cell, pbc=True)
if wrap:
    prim_atoms.wrap()

##### Writing Primitive Structure #######
fname_out = make_filename(fname, label=label)
io.write(fname_out, prim_atoms)

##### printing info ######
if print_full_info:
    for key in data:
        print(key,":" )
        print(data[key])
else:
    print_basic_info(data)


special_path   = to_ase_path_string(data['path'])
special_points = to_ase_special_points(data['point_coords'])

print('special_points:')
for key in special_points.keys():
    print(key, special_points[key])
print('special_path:',special_path)

##### Writing bandpath from special points and special path ######
write_bandpath_to_json(bandpath_filename, special_path, special_points, prim_atoms)

##### just to ensure the io process works #####
#in_special_path, in_special_points = read_bandpath_from_json(bandpath_filename)
#for key in in_special_points:
#    print(key, ':', in_special_points[key])
#print(in_special_path)
