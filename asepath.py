#!/usr/bin/env python3



divider = ' -- ' 

remove_translation = False

force_consistent = True
import sys
from ase import io
import numpy as np
######################

if len(sys.argv) <2:
    raise Exception("No input files given!")

fnames = sys.argv[1:]

if fnames[0] == '-rt' or fnames[0] == '--remove-translation':
    remove_translation = True
    del fnames[0]
    
    
if len(fnames) == 0:
    raise Exception("No input files given!")



###################
def get_rms_force(forces):
    fmag2 =  (forces**2).sum(axis=1)
    rms_force = np.sqrt(np.mean(fmag2))
    return rms_force

def get_max_force(forces):
    fmag = np.sqrt( (forces**2).sum(axis=1))
    smap = np.argsort(fmag)
    atom_index_max_force = smap[-1]
    max_force = fmag[atom_index_max_force]
    return max_force, atom_index_max_force

def is_matching_structure(atoms_a, atoms_b):
    if len(atoms_a)==len(atoms_b):
        ela = atoms_a.get_chemical_symbols()
        elb = atoms_b.get_chemical_symbols()
        matches = [ela[i]==elb[i] for i in range(len(atoms_a))]
        ismatch = all(matches)
    else:
        ismatch = False
    return ismatch


def get_unit_vec(vec):
    norm = np.linalg.norm(vec)
    if norm == 0.0:
        unit_vec = vec*0.0
    else:
        unit_vec = vec/norm
    return unit_vec

from ase.geometry import find_mic
def get_motion_unit_vec_and_distance(atoms_a, atoms_b):
    D = atoms_a.positions - atoms_b.positions # 2d arrays
    vec, distances = find_mic( D, atoms_a.cell )
    if remove_translation:
        vec = vec - np.mean(vec, axis=0)
    distance = np.linalg.norm(vec)
    unit_vec = get_unit_vec(vec)
    return unit_vec, distance

def get_angle(unit_vec, unit_vec_old):
    eps = 1e-8 # to prevent overflows in arccos
    angle = np.arccos( (unit_vec*unit_vec_old).sum()/(1+eps) )* 180/np.pi
    #gives appropriate sign
    angle = angle * np.linalg.norm(unit_vec) * np.linalg.norm(unit_vec_old) 
    return angle

def get_energy_safe(image):
    try:
        energy = image.get_potential_energy(
            force_consistent=force_consistent)
    except:
        energy = image.calc.results['energy']
        
    return energy

####################
for fi, fname in enumerate(fnames):

    if 'traj' in fname:
        images = io.Trajectory(fname,'r')
    else:
        images = io.read(fname, index = ':')
    
    for i, image in enumerate(images):

        if fi == 0 and i == 0: # only on first total image
            previous_image = image
            previous_unit_vec, previous_distance = 0.0, 0.0
            total_distance = 0.0
        if not is_matching_structure(image, previous_image):
            previous_image = image
            previous_unit_vec, previous_distance = 0.0, 0.0
            total_distance = 0.0
            
        ######## now for tests ######
        
        ### energy tests 
        if image.calc is not None:
            energy = get_energy_safe(image)
            if previous_image.calc is not None:
                denergy = energy - get_energy_safe(previous_image)
            else:
                denergy = 0.0
        else:
            energy, denergy = 0.0, 0.0
        
        ### force tests 
        if image.calc is not None:
            forces = image.get_forces()
            rms_force = get_rms_force(forces)
            max_force, atom_index_max_force = get_max_force(forces)
        else:
            rms_force, max_force, atom_index_max_force = 0.0, 0.0, 0
        
        ### path directions
        unit_vec, distance = get_motion_unit_vec_and_distance(image, previous_image)
        total_distance += distance
        angle = get_angle(unit_vec, previous_unit_vec)
        
        if previous_distance > 0.0:
            forward_projection = np.cos(angle * np.pi/180) * distance/previous_distance
        else:
            forward_projection = 0.0

        print(fname,
            '{:4d}'.format(i),
            'E {: 14.9f} dE {: 13.9f}'.format(energy ,denergy),
            'rmsF {:.3e} ({:.3e} on {:4d})'.format(rms_force, max_force, atom_index_max_force),
            'dX {:.6f} |X| {:.4f} fp {: 6.2f} ∠ {:5.1f}°' .format(distance, total_distance, forward_projection, angle ))
               
    
        previous_image = image
        previous_distance = distance
        previous_unit_vec = unit_vec

    # print a divider only when between files
    if fi < (len(fnames)-1) and len(divider)>0:  print(divider)
