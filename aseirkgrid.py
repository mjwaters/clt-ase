#!/usr/bin/env python3


# usage: aseirkgrid.py filename  k1 k2 k3  (-gamma) (-symprec 1e-5) (-all)



import argparse
parser = argparse.ArgumentParser()
## these are required
parser.add_argument("filename", help="Your structure file.")
parser.add_argument("nkp1", type=int, help="# k-points along axis 1")
parser.add_argument("nkp2", type=int, help="# k-points along axis 2")
parser.add_argument("nkp3", type=int, help="# k-points along axis 3")
## these are options
parser.add_argument("-gamma", help="Enforce a Gamma centered grid", action="store_true")
parser.add_argument("-all", help="Show all (kpt -> ir_kpt) mappings", action="store_true")
parser.add_argument("-symprec", type=float, help="Change the symprec used for symmetry analysis. Default: 1e-5")
###
args = parser.parse_args()




filename = args.filename
mh_grid = (int(args.nkp1), int(args.nkp2), int(args.nkp3))

gamma = args.gamma
show_all = args.all
if args.symprec is None:
    symprec = 1e-5 # the vasp default
else:
    symprec = args.symprec
#######################
from ase import io

atoms = io.read(filename)

# converting for spglib
lattice   = atoms.get_cell()
positions = atoms.get_scaled_positions()
numbers   = atoms.get_atomic_numbers()
cell = (lattice, positions, numbers)

##############
if gamma:
    grid_shift = ( 0, 0, 0)
else:
    grid_shift = (-0.5,-0.5, -0.5)

########################################################
import spglib
import numpy as np
#kgrid = mh_grid
shift = 2*np.array(grid_shift)

########################################################
mapping, bz_gp = spglib.get_ir_reciprocal_mesh(mh_grid, cell, is_shift=shift) #is_shift is in half bzgrid cells?

##########  Printing
# some k-points and mapping to ir-grid points
irr_kpts = np.unique(mapping)
num_irr_kpts = len(irr_kpts)
print('With symprec = %.2e, there are [%d] irr k-points.'%(symprec,num_irr_kpts))


gp_max_print = 32 # this is default max, overide with the -all option!
if show_all or len(mapping) <= gp_max_print: 
    bzmax = len(mapping)
    print('All %i (kpt -> ir_kpt) mappings:'%len(mapping))
else:
    bzmax = gp_max_print
    print('First %i of %i (kpt -> ir_kpt) mappings:'%(bzmax, len(mapping)) )

for bz_index in range(0,bzmax):

    ir_bz_index = mapping[bz_index] #also what im using for set_num
    
    gp         =    bz_gp[bz_index]
    ir_gp    =   bz_gp[ir_bz_index]
    
    kp       = (gp.astype(float) + shift/2) / mh_grid
    ir_kp = (ir_gp.astype(float) + shift/2) / mh_grid
    

    fstr = "%3d gp %s kp %s"
    gps =   fstr%(    bz_index,    str(gp).ljust(13),    kp.round(8))
    ir_gps= fstr%( ir_bz_index, str(ir_gp).ljust(13),   ir_kp.round(8))
    
    #fstr = "%3d gp %s %s kp %s"
    #shifted_gp    = np.array(   gp)#+np.array(gp_shift)
    #shifted_ir_gp = np.array(ir_gp)#+np.array(gp_shift)
    #gps =   fstr%(    bz_index,    str(gp).ljust(13), str(shifted_gp).ljust(5),        kp.round(8))
    #ir_gps= fstr%( ir_bz_index, str(ir_gp).ljust(13), str(shifted_ir_gp).ljust(13),   ir_kp.round(8))
    
    print("%s -> %s" % (gps.ljust(45),ir_gps ))
