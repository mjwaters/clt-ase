#!/usr/bin/env python3


# need to make these into arg parser options

use_standard_primitive = True
KPM = 'SC'


print_band_path_before_conversion = False
########
if use_standard_primitive:
    label = 'pmg_'+KPM+'.prim_std'
else:
    label = 'pmg_'+KPM+'.prim'

bandpath_filename =  'bandpath.%s.json'% label

#################
from pymatgen.core import Structure
from pymatgen.io.ase import AseAtomsAdaptor
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from pymatgen.symmetry.kpath import KPathSetyawanCurtarolo, KPathLatimerMunro, KPathSeek
import sys
import os

def make_filename(fname_in, label):
    fname = os.path.basename(fname_in) # so the json and structure go to the same place
    i = 0
    for _ in range(len(fname)):
        if fname[_]=='.':
            i=_
    outname = fname[0:i] + '.'+label+fname[i:]
    return outname


def convert_SC_kpt_symbol_to_ase(sym):
    if sym == '\\Gamma':
        return 'G'
    if '_' in sym:
         return sym.replace('_','')
    return sym

def convert_LM_kpt_symbol_to_ase(sym):
    if sym == 'Γ':
        return 'G'
    if 'g' == sym[0]:
        symout = 'g0'
    elif '_' in sym:
         symout = sym.replace('_{','')
         symout = symout.replace('}','')
    else:
        symout = sym
    return symout.upper()

def atoms_to_dict(atoms):
    adict =  atoms.todict()
    #'symbols'          = atoms.get_chemical_symbols()
    #'scaled_positions' = atoms.get_scaled_positions()
    #'cell'             = atoms.get_cell()
    #'pbc'              = atoms.get_pbc()

    for key in adict: # i hope this always works so I don't have to do something ugly like above
        adict[key] = adict[key].tolist()
    return adict

def convert_pymatgen_special_path_to_ase(path, sym_conv_func):

    def point_segment_list_to_string(seg):
        strout = ''
        for sp_sym in seg:
            strout+=sym_conv_func(sp_sym)
        return strout

    bpstring = point_segment_list_to_string(path[0])
    if len(path)>1:
        for seg in path[1:]:
            bpstring+=','+point_segment_list_to_string(seg)
    return bpstring

def convert_pymatgen_special_points_to_ase(kpoints, sym_conv_func):
    '''kpoints special points are a dict'''
    special_points ={}

    for key in kpoints:
        sym = sym_conv_func(key)
        kpt = [float(val) for val in kpoints[key] ] #force dtype of float for json
        special_points[sym] = kpt

    return special_points


def write_bandpath_to_json(fname, special_path, special_points, atoms=None,
    sanitize_kpt_vectors=True):
    # ex file name ='bandpath.json'

    if sanitize_kpt_vectors:
        spout = {}
        for key in special_points:
            kpt = [float(val) for val in special_points[key] ] #force dtype of float for json
            spout[key] = kpt
    else:
        spout = special_points

    dout = {'special_path'   : special_path,
            'special_points' : spout}
    if atoms is not None:
        dout['atoms'] = atoms_to_dict(atoms)
    import json
    with open(fname, 'w') as f:
        json.dump(dout, f, indent=4,  separators=(',',':'))

### handy for swapping
KPathMethods = {
    'SC': KPathSetyawanCurtarolo,
    'LM': KPathLatimerMunro}

sym_conv_funcs = {
    'SC': convert_SC_kpt_symbol_to_ase,
    'LM': convert_LM_kpt_symbol_to_ase}

##########################################################################


##### reading structure #######
fname=sys.argv[-1]
structure = Structure.from_file(fname)
SPGA = SpacegroupAnalyzer(structure)

if use_standard_primitive:
    prim = SPGA.get_primitive_standard_structure(international_monoclinic=False)
else:
    prim = SPGA.find_primitive()


########
fname_out = make_filename(fname, label=label)
prim.to(filename=fname_out, fmt='poscar')
#######

kpath_prim = KPathMethods[KPM](prim)
sym_conv_func = sym_conv_funcs[KPM]


kpath = kpath_prim.kpath['path']
kpoints = kpath_prim.kpath['kpoints']

if print_band_path_before_conversion:
    print('special_points:')
    for key in kpoints.keys():
        print(key, kpoints[key])
    print('special_path',kpath)

special_path   = convert_pymatgen_special_path_to_ase(kpath, sym_conv_func)
special_points = convert_pymatgen_special_points_to_ase(kpoints, sym_conv_func)

###
print('special_points:')
for key in special_points.keys():
    print(key, special_points[key])
print('special_path:',special_path)

####
atoms_prim = AseAtomsAdaptor.get_atoms(prim)
write_bandpath_to_json(bandpath_filename, special_path, special_points, atoms_prim)
